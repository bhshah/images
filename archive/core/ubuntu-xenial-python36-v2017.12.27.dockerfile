FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-xenial-python36

ENV D3M_INTERFACE_VERSION=v2017.12.27

RUN \
 git clone --recursive https://gitlab.com/datadrivendiscovery/metadata.git -b $D3M_INTERFACE_VERSION && \
 cd metadata && \
 pip3 install -r requirements.txt && \
 pip3 install . && \
 cd .. && \
 rm -rf metadata && \
 git clone --recursive https://gitlab.com/datadrivendiscovery/primitive-interfaces.git -b $D3M_INTERFACE_VERSION && \
 cd primitive-interfaces && \
 pip3 install -r requirements.txt && \
 pip3 install . && \
 cd .. && \
 rm -rf primitive-interfaces && \
 git clone --recursive https://gitlab.com/datadrivendiscovery/d3m.git -b $D3M_INTERFACE_VERSION && \
 cd d3m && \
 pip3 install -r requirements.txt && \
 pip3 install . && \
 cd .. && \
 rm -rf d3m && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
