FROM registry.gitlab.com/datadrivendiscovery/images/shared:ubuntu-bionic-python38

# This list should be kept in sync with FIXED_PACKAGE_VERSIONS in
# https://gitlab.com/datadrivendiscovery/primitives/-/blob/master/run_validation.py.

# Install TensorFlow GPU version, and appropriate versions of Keras, Theano, Pytorch, mxnet.
# We also install other fixed versions of ML libraries. Should be kept in sync with
# "run_validation.py" script in primitives repository. Cython is installed in the base image.
RUN pip3 install \
 numpy==1.18.2 && \
 pip3 install \
 scipy==1.4.1 && \
 pip3 install \
 pandas==1.1.3 && \
 pip3 install \
 networkx==2.4 \
 Pillow==7.1.2 \
 hyppo==0.1.3 \
 selenium==3.141.0 \
 tqdm==4.48.2 && \
 pip3 install \
 scikit-learn==0.22.2.post1 && \
 pip3 install \
 tensorflow-gpu==2.2.0 \
 Keras==2.3.1 \
 torch==1.7.0 \
 torchvision==0.8.1 \
 sentencepiece==0.1.91 \
 Theano==1.0.4 \
 mxnet==1.6.0 \
 shap==0.37.0 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
