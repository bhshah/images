A repository with multiple Docker images used in the D3M program.

If you are just starting and do not know which Docker image to use,
use `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-master`.
See the [Docker image UI](https://docs.datadrivendiscovery.org/docker.html) for the list of stable (released)
images and the list of all images and their tags in a more organized way.

## Structure

Currently supported images are:

* `registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36` — Base Docker image with Python 3.6 and general build dependencies.
* `registry.gitlab.com/datadrivendiscovery/images/shared:ubuntu-bionic-python36` — Docker image based on `base`, with CUDA.
* `registry.gitlab.com/datadrivendiscovery/images/libs:ubuntu-bionic-python36` — Docker image based on `shared`, with various ML libraries with fixed versions.
* `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-master` — Docker image extending `libs` with stable version of [core D3M Python package](https://gitlab.com/datadrivendiscovery/d3m) installed (`master` branch).
* `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-devel` — Docker image extending `libs` with development version of [core D3M Python package](https://gitlab.com/datadrivendiscovery/d3m) installed (`devel` branch).
* `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-master` — Docker image based on `core` (`master`), with all primitives submitted to
  [open source primitives index](https://gitlab.com/datadrivendiscovery/primitives) installed.
* `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-devel` — Docker image based on `core` (`devel`), with all primitives submitted to
  [open source primitives index](https://gitlab.com/datadrivendiscovery/primitives) installed.
* `registry.gitlab.com/datadrivendiscovery/images/libs-lite:ubuntu-bionic-python36` — A lite version of the `libs` image without Docker-in-Docker, CUDA, and large ML libraries. Based on `base` only.
* `registry.gitlab.com/datadrivendiscovery/images/testing:ubuntu-bionic-python36` — Docker image based on `shared`, with Docker-in-Docker, Kubernetes-in-Docker, Go, JavaScript/node.js used for CI testing in other repositories.
* `registry.gitlab.com/datadrivendiscovery/images/testing-lite:ubuntu-bionic-python36` — A lite version of the `testing` image without Docker-in-Docker, Kubernetes-in-Docker, and CUDA. Based on `base` only.
* `registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic` — A small testing image with only Docker-in-Docker, without Python 3.6.

Note: `master` and `devel` images are mostly the same, the only difference is which version of the core D3M Python package is installed (`devel` images enable
testing against the upcoming core D3M Python package release). You probably want to use `master` image, unless you need some new features from the core D3M
Python package, or you want to validate that your code works against the future release.

Note: Both `master` and `devel` primitives images can be unstable (as in "can include changes in flux") because they install primitives from the
`master` branch of the [open source primitives index](https://gitlab.com/datadrivendiscovery/primitives) which might be in a state of flux itself.
If you want a stable (released) image, pick one from [the list of stable images](https://docs.datadrivendiscovery.org/docker.html).

![Diagram of the images and their dependencies](diagram.png)

Images do change through time because base images change with security and other similar updates
to system packages and dependencies, or primitives are installed or updated.
To support complete reproducibility, each built image is also tagged with a timestamp-based
tag which is never overridden with a different image. The tag is of the form
`registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-<type>-<timestamp>`.

For example, instead of using `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-master`
image tag, use, for example,
`registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-master-20201122-083834`.
This image tag points to a concrete version of an image build at one point in time.

## GPU support

Docker images are made in a way that they can run both with or without GPU support. If you want primitives to have GPUs available and use them,
see [mode information here how to run the image](https://github.com/NVIDIA/nvidia-docker). Otherwise, they will run without GPUs.

## License Agreements

By downloading these images, you agree to the terms of the license agreements for NVIDIA software included in the images.
